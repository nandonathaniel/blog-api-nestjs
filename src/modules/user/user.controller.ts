import { Controller, Get, Post, Body, Patch, Param, Delete, BadRequestException, ParseIntPipe, ValidationPipe, UseGuards, Req, UnauthorizedException } from '@nestjs/common';
import { UserService } from './user.service';
import { UserRequestDto } from './dto/user-request.dto';
import internal from 'stream';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { UserRole } from './entities/user.entity';
import { Roles } from './decorators/roles.decorators';
import { RolesGuard } from './guards/roles.guard';

@Controller('user')
@ApiTags('User')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  create(@Body() userRequestDto: UserRequestDto) {
    if (!userRequestDto.username || !userRequestDto.password || !userRequestDto.name || !userRequestDto.role) {
      throw new BadRequestException('Missing required properties');
    }
    const alphanumericRegex = /^(?=.*[a-zA-Z])[a-zA-Z0-9]+$/;
    if (!alphanumericRegex.test(userRequestDto.username)) {
      throw new BadRequestException('Username must be alphanumeric with at least one alphabetical character');
    }
    return this.userService.create(userRequestDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get()
  findAll(@Req() req) {
    // console.log("HEHEHEHHEHEHEHEHE");
    const isAdmin = req.user?.role === UserRole.Admin;
    return isAdmin ? this.userService.findAll() : this.userService.findAllWithoutAdmin();
  }

  // @Get()
  // findAll() {
  //   return this.userService.findAll()
  // }

  @Get(':id(\\d+)')
  findById(@Param('id', ParseIntPipe) id: number) {
    // console.log("Hello");
    return this.userService.findOneId(+id);
  }

  @Get(':username')
  findByUsername(@Param('username') username: string) {
    // console.log("username",username);
    const regex = /^[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*$/;
    if (!regex.test(username)) {
      throw new Error('Invalid username');
    }
    return this.userService.findOneUsername(username);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id(\\d+)')
  update(@Req() req, @Param('id', ParseIntPipe) id: number, @Body() userRequestDto: UserRequestDto) {
    const userId = req.user.sub;

    const isAdmin = req.user.role == UserRole.Admin;
    const isSameUser = userId === id;

    if (!isAdmin && !isSameUser) {
      throw new UnauthorizedException('You are not authorized to update this user.');
    }

    if (!isAdmin && userRequestDto.role) {
      userRequestDto.role = UserRole.Member;
    }
    
    return this.userService.update(id, userRequestDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.Admin)
  @Delete(':id(\\d+)')
  remove(@Req() req, @Param('id', ParseIntPipe) id: number) {
    // console.log(req);
    return this.userService.remove(+id);
  }
}
