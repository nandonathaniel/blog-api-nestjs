import { Module, forwardRef } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { RolesGuard } from './guards/roles.guard';
import { APP_GUARD } from '@nestjs/core';
import { User } from './entities/user.entity';
import { BlogModule } from '../blog/blog.module';

@Module({
  imports: [BlogModule, TypeOrmModule.forFeature([User])],
  controllers: [UserController],
  providers: [UserService, UserRepository, /*RolesGuard, {
    provide: APP_GUARD,
    useClass: RolesGuard,
  },*/],
  exports: [UserService, UserRepository]
})
export class UserModule {}
