import { Inject, Injectable, NotFoundException, forwardRef} from '@nestjs/common';
import { UserRequestDto } from './dto/user-request.dto';
import { UserRepository } from './user.repository';
import { BlogService } from '../blog/blog.service';
import { encodePassword } from '../../utils/bcrypt';

@Injectable()
export class UserService {
  constructor(
    private usersRepository: UserRepository,
    private readonly blogService: BlogService
  ) {}
  async create(userRequestDto: UserRequestDto) {
    // console.log("HAHDFAHFASHBFA");
    const password = await encodePassword(userRequestDto.password);
    return this.usersRepository.create(userRequestDto, password);
  }

  findAll(){
    return this.usersRepository.findAll();
  }

  findAllWithoutAdmin(){
    return this.usersRepository.findAllWithoutAdmin();
  }

  async findOneId(id: number) {
    // console.log("HELLO GUYS");
    const result = await this.usersRepository.findOneId(id);
    if (!result) {
      throw new NotFoundException('User not found');
    }
    return result;
  }

  async findOneUsername(username: string){
    // console.log("username2 ",username);
    const result = await this.usersRepository.findOneUsernameExceptPassword(username);
    if (!result) {
      throw new NotFoundException('User not found');
    }
    return result;
  }

  async findOneUsernameWithPassword(username: string){
    const result = await this.usersRepository.findOneUsername(username);
    if (!result) {
      throw new NotFoundException('User not found');
    }
    return result;
  }

  async remove(id: number){
    // console.log("ADA KOK");
    const result = await this.usersRepository.delete(id);
    if(result.affected == 0) {
      throw new NotFoundException('User not found');
    }
    else {
      this.blogService.removeByUserId(id);
      return "Deleted User with id " + id.toString();
    }
    
  }

  async update(id: number, userRequestDto: UserRequestDto) {
    if(userRequestDto.password){
      userRequestDto.password = await encodePassword(userRequestDto.password);
    }

    const result = await this.usersRepository.update(id, userRequestDto);
    if(result.affected == 0) {
      throw new NotFoundException('User not found');
    }
    else {
      return this.findOneId(id);
    }
  }
}
