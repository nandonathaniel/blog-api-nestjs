import {Inject, Injectable, NotFoundException, UnauthorizedException, forwardRef} from '@nestjs/common';
import { BlogRequestDto} from './dto/blog-request.dto';
import { UserService } from '../user/user.service';
import { BlogRepository } from './blog.repository';
import { BlogRequestDto2 } from './dto/blog-request-2.dto';
import { join } from 'path';
import { createReadStream, statSync } from 'fs';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class BlogService {
  
  constructor(
    private blogsRepository: BlogRepository,
    // @Inject(forwardRef(() => UserService))
    // private readonly userService: UserService
  ) {}
  
  async create(userId: number, blogRequestDto: BlogRequestDto2) {
    return this.blogsRepository.create(userId, blogRequestDto);
  }

  findAll(){
    return this.blogsRepository.findAll();
  }

  async findOne(id: number) {
    const result = await this.blogsRepository.getBlogById(id);
    if (!result) {
      throw new NotFoundException('Blog not found');
    }
    return result;
  }

  async remove(isAdmin: boolean,id: number, userId: number){
    
    const blog = await this.findOne(id);
    if (!blog) {
      throw new NotFoundException('Blog not found');
    }
    // console.log(userId);
    // console.log(blog.userId);
    if (blog.userId !== userId && !isAdmin) {
      throw new UnauthorizedException("You don't have permission to delete this blog");
    }

    const result = await this.blogsRepository.delete(id);
    if(result.affected == 0) {
      throw new NotFoundException('Blog not found');
    }
    else {
      return "Deleted Blog with id " + id.toString();
    }
    
  }

  async update(isAdmin: boolean, id: number, userId: number, blogRequestDto: BlogRequestDto2) {

    const blog = await this.findOne(id);
    if (!blog) {
      throw new NotFoundException('Blog not found');
    }

    if (blog.userId !== userId && !isAdmin) {
      throw new UnauthorizedException("You don't have permission to update this blog");
    }

    const result = await this.blogsRepository.update(id, blogRequestDto);
    if(result.affected == 0) {
      throw new NotFoundException('Blog not found');
    }
    else {
      return this.findOne(id);
    }
  }

  async removeByUserId(id: number) {
    const blogs = await this.blogsRepository.getAllBlogsByUser(id);
      for (const blog of blogs) {
        await this.blogsRepository.delete(blog.id);
      }
  }

  async getAllBlogsByUser(id: number) {
    return await this.blogsRepository.getAllBlogsByUser(id);
  }

  // async findOneUsername(username: string) {
  //   try {
  //     const user = await this.userService.findOneUsername(username);
  //     return await this.blogsRepository.getAllBlogsByUser(user.id);
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  @Cron('*/30 * * * *')
  async publishScheduledBlogs() {
    console.log("MASUKKKKK");
    await this.blogsRepository.publishAllUnpublishedBlogs();
  }

}
