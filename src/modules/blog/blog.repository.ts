import { Injectable} from '@nestjs/common';
import { BlogRequestDto} from './dto/blog-request.dto';
import {Blog} from './entities/blog.entity'
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { BlogRequestDto2 } from './dto/blog-request-2.dto';
import env from '../../config/env';
import { checkAndMove} from '../../utils/checkIfFileExists';
import { BlogType } from './enums/type.enum';

@Injectable()
export class BlogRepository {
    @InjectRepository(Blog)
    private blogsRepository: Repository<Blog>;

    constructor(db: DataSource) {
        this.blogsRepository = db.getRepository(Blog);
    }

    async clear() {
        console.log("blog ke clear");
        const allBlogs = await this.blogsRepository.find();
        await this.blogsRepository.remove(allBlogs);
    }

    delete(id: number) {
        return this.blogsRepository.softDelete(id);
    }

    findAll() {
        return this.blogsRepository.find({
            where: {
                isPublished: true,
            },
        });
    }

    findAllUnpublishedBlogs() {
        return this.blogsRepository.find({
            where: {
                isPublished: false,
            },
        });
    }

    save(blog: Blog) {
        this.blogsRepository.save(blog);
    }

    async create(userId: number,blogRequestDto: BlogRequestDto2) {
        const exists = await checkAndMove(blogRequestDto.imageFileName);

        if(!exists) {
            throw new Error('Image not found in the "temporary" folder.');
        }

        let theUrl = null;
        if(blogRequestDto.imageFileName){
            theUrl = "http://localhost:" + env.PORT + "/persistent/" + blogRequestDto.imageFileName;
        }

        const { type, ...blogData } = blogRequestDto;

        const blog = this.blogsRepository.create({
            userId: userId,
            ...blogData,
            imageUrl: theUrl,
            createdDate: new Date(),
            isPublished: blogRequestDto.type !== BlogType.Scheduled,
        });

        return this.blogsRepository.save(blog);
    }

    // create2(userId: number, title: string, content: string, image: string) {
    //     const blog = this.blogsRepository.create({
    //         userId: userId,
    //         title: title,
    //         content: content,
    //         image: "http://localhost:" + env.PORT + "/" + image || null,
    //         createdDate: new Date()
    //     });
    //     return this.blogsRepository.save(blog);
    // }

    async update(id: number, blogRequestDto: BlogRequestDto2) {
        let theUrl = null;
        if(blogRequestDto.imageFileName) {
            const exists = await checkAndMove(blogRequestDto.imageFileName);

            if(!exists) {
                throw new Error('Image not found in the "temporary" folder.');
            }

            theUrl = "http://localhost:" + env.PORT + "/persistent/" + blogRequestDto.imageFileName;
        }
        
        const { type, ...blogData } = blogRequestDto;

        const updateData = {
            ...blogData,
            createdDate: new Date(),
            imageUrl: theUrl 
        }
        const result = await this.blogsRepository.update(
            { id },
            blogRequestDto.type ? { ...updateData, isPublished: blogRequestDto.type !== BlogType.Scheduled } : updateData
        );
        return result;
    }

    async getAllBlogsByUser(id: number) {
        // const blogs = await this.blogsRepository
        // .createQueryBuilder('blog')
        // .leftJoinAndSelect('blog.user', 'user')
        // .where('blog.user = :id', { id })
        // .getMany();
        

        const blogs2 = await this.blogsRepository.find({
            where: {
              userId: id,
              isPublished: true
            },
        });
        console.log(blogs2);

        return blogs2;
    }

    getBlogById(id: number) {
        return this.blogsRepository.findOne({
            where: {
                id: id,
                isPublished: true
            }
        });
    }

    async publishAllUnpublishedBlogs() {
        await this.blogsRepository.update({ isPublished: false }, { isPublished: true });
    }
}

