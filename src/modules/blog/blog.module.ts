import { Module, forwardRef } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogController } from './blog.controller';
import { Blog } from './entities/blog.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogRepository } from './blog.repository';
import { PassportModule } from '@nestjs/passport';
import { RolesGuard } from '../user/guards/roles.guard';
import { APP_GUARD } from '@nestjs/core';
import { UserModule } from '../user/user.module';

@Module({
  controllers: [BlogController],
  providers: [BlogService, BlogRepository, /*RolesGuard, {
    provide: APP_GUARD,
    useClass: RolesGuard,
  },*/],
  imports: [/*forwardRef(() => UserModule),*/ TypeOrmModule.forFeature([Blog]), PassportModule],
  exports: [BlogService, BlogRepository]
})
export class BlogModule {}
