import { ApiProperty } from "@nestjs/swagger";

export class BlogRequestDto {
    @ApiProperty({
        example : "Example Title"
    })
    title: string;
    @ApiProperty({
        example : "Example Content"
    })
    content: string;
}
