import { Controller, Get, Post, Body, Patch, Param, Delete, BadRequestException, Req, UseGuards, UnauthorizedException, ParseIntPipe, UseInterceptors, UploadedFile, Res, NotFoundException, Version } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogRequestDto } from './dto/blog-request.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { RolesGuard } from '../user/guards/roles.guard';
import { UserRole } from '../user/enums/role.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import {v4 as uuidv4} from 'uuid';
import { diskStorage } from 'multer';
import { multerConfig } from '../../config/multer.config';
import { BlogRequestDto2 } from './dto/blog-request-2.dto';
import { createReadStream, statSync } from 'fs';
import { join } from 'path';
import { Observable, of } from 'rxjs';

@Controller('blog')
@ApiTags('Blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}

  
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Version('1')
  @Post()
  // @Post("v1")
  create(@Req() req, @Body() blogRequestDto: BlogRequestDto2) {
    const userId = req.user.sub;
    if (!blogRequestDto.title || !blogRequestDto.content) {
      throw new BadRequestException('Missing required properties');
    }
    return this.blogService.create(userId, blogRequestDto);
  }

  
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Version('2')
  @Post()
  // @Post("v2")
  create2(@Req() req, @Body() blogRequestDto: BlogRequestDto2) {
    const userId = req.user.sub;
    // console.log(blogRequestDto.image);
    if (!blogRequestDto.title || !blogRequestDto.content || !blogRequestDto.imageFileName) {
      throw new BadRequestException('Missing required properties');
    }
    return this.blogService.create(userId, blogRequestDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Version('3')
  @Post()
  // @Post("v2")
  create3(@Req() req, @Body() blogRequestDto: BlogRequestDto2) {
    const userId = req.user.sub;
    // console.log(blogRequestDto.image);
    if (!blogRequestDto.title || !blogRequestDto.content || !blogRequestDto.imageFileName || !blogRequestDto.type) {
      throw new BadRequestException('Missing required properties');
    }
    return this.blogService.create(userId, blogRequestDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('me')
  getAllBlogsByMe(@Req() req) {
    const userId = req.user.sub;
    return this.blogService.getAllBlogsByUser(userId);
  }

  @Get(':id(\\d+)')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.blogService.findOne(+id);
  }

  // @Get(':username')
  // findBlogByUsername(@Param('username') username: string) {
  //   console.log("username",username);
  //   const regex = /^[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*$/;
  //   if (!regex.test(username)) {
  //     throw new Error('Invalid username');
  //   }
  //   return this.blogService.findOneUsername(username);
  // }

  @Get()
  findAll() {
    return this.blogService.findAll();
  }


  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch(':id(\\d+)')
  update(@Req() req, @Param('id', ParseIntPipe) id: number, @Body() blogRequestDto: BlogRequestDto2) {
    const userId = req.user.sub;

    const isAdmin = req.user.role == UserRole.Admin;

    return this.blogService.update(isAdmin, +id, userId, blogRequestDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id(\\d+)')
  remove(@Req() req, @Param('id', ParseIntPipe) id: number) {
    const userId = req.user.sub;

    const isAdmin = req.user.role == UserRole.Admin;

    return this.blogService.remove(isAdmin, +id, userId);
  }

  @Post('upload')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file', multerConfig))
  uploadFile(@UploadedFile('file') file) {
    console.log(file);
    // Assuming you have set up a proper path for static files in 'serve-static' configuration
    const imageFileName = file.filename;
    return { imageFileName };
  }

  // @Get(':photoId')
  // async getImage(@Param('photoId') photoId: string, @Res() res: Response) {

  //   return of(res.sendFile(join(process.cwd(), 'temporary/'+photoId)));
  // }
}
