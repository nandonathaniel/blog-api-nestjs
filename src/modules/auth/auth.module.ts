import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from '../user/guards/roles.guard';
import { UserModule } from '../user/user.module';
const passportStrategy = PassportModule.register({ defaultStrategy: 'jwt' });
@Module({
  imports: [
    passportStrategy,
    UserModule,
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '90d' },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, /*{
    provide: APP_GUARD,
    useClass: RolesGuard,
  },*/],
  exports: [AuthService, passportStrategy]
})
export class AuthModule {}
