import { ApiProperty } from "@nestjs/swagger";

export class LogInDto {
    @ApiProperty({
        example : "exampleUsername"
    })
    username: string;
    @ApiProperty({
        example : "examplePassword"
    })
    password: string;
}
