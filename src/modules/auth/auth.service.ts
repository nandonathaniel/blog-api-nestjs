import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import { comparePasswords } from '../../utils/bcrypt';

@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UserService,
        private jwtService: JwtService

    ) {}

    async signIn(username: string, pass: string) {
        try {
            const user = await this.userService.findOneUsernameWithPassword(username);
            console.log(user);
            const matched = comparePasswords(pass, user.password);
            if(!matched) {
                if (pass !== user.password) {
                    throw new UnauthorizedException('Invalid credentials');
                }
            }
            const payload = { sub: user.id, username: user.username, role: user.role };
            return {
                access_token: await this.jwtService.signAsync(payload),
            };
        } catch (error) {
            throw error;
        }
    }

    validateUser(payload: any) {
        return payload;
    }
}
