import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './modules/user/user.module';
import { BlogModule } from './modules/blog/blog.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './modules/auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import env from './config/env';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './modules/user/guards/roles.guard';
import { MulterModule } from '@nestjs/platform-express';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ScheduleModule } from '@nestjs/schedule';
import { RequestLoggingMiddleware } from './common/middleware/request-logging.middleware';

@Module({
  imports: [ConfigModule.forRoot({isGlobal:true}), UserModule, BlogModule, TypeOrmModule.forRoot({
    type: 'mysql',
    host: process.env.DATABASE_HOST,
    port: env.DATABASE_PORT,
    username: env.DATABASE_USERNAME,
    password: env.DATABASE_PASSWORD,
    database: env.DATABASE_NAME,
    entities: [__dirname + '/../**/*.entity.js'],
    synchronize: true,
    logging: ["error", "query"]
  }), AuthModule, MulterModule.register({ dest: 'path kamu' }),ServeStaticModule.forRoot({
    rootPath: join(process.cwd(), 'storage'), 
  }), ScheduleModule.forRoot()],
  controllers: [AppController],
  providers: [
    AppService,
    /*{
      provide: APP_GUARD,
      useClass: RolesGuard,
    },*/
    // Other providers and modules...
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    // Apply the request logging middleware to all routes
    consumer.apply(RequestLoggingMiddleware).forRoutes('*');
  }
}
