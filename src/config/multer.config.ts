import { diskStorage } from 'multer';
import { extname, parse } from 'path'; // Import the 'parse' function from 'path'
import { v4 as uuidv4 } from 'uuid';

export const multerConfig = {
  storage: diskStorage({
    destination: './storage/temporary/', // Define the temporary upload destination
    filename: (req, file, cb) => {
      const filename: string = parse(file.originalname).name.replace(/\s/g, '') + uuidv4(); // Use 'parse' here
      const extension: string = extname(file.originalname);
      cb(null, `${filename}${extension}`);
    },
  }),
};
