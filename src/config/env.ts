import * as dotenv from "dotenv"

dotenv.config()

const env = {
    PORT : process.env.PORT?parseInt(process.env.PORT):3001,
    DATABASE_HOST : process.env.DATABASE_HOST,
    DATABASE_PORT : process.env.DATABASE_PORT?parseInt(process.env.DATABASE_PORT):3306,
    DATABASE_USERNAME : process.env.DATABASE_USERNAME,
    DATABASE_PASSWORD : process.env.DATABASE_PASSWORD,
    DATABASE_NAME : process.env.DATABASE_NAME,
    DATABASE_NAME_TESTING : process.env.DATABASE_NAME_TESTING,
    SECRET_JWT_CONSTANTS : process.env.SECRET_JWT_CONSTANS
}

export default env;