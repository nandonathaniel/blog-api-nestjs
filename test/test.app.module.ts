import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { UserModule } from '../src/modules/user/user.module';
import { BlogModule } from '../src/modules/blog/blog.module';
import env from '../src/config/env';
import { AuthModule } from '../src/modules/auth/auth.module';
import { AppController } from '../src/app.controller';
import { AppService } from '../src/app.service';
import { User } from '../src/modules/user/entities/user.entity';
import { Blog } from '../src/modules/blog/entities/blog.entity';

@Module({
  imports: [ConfigModule.forRoot({isGlobal:true}), UserModule, BlogModule, TypeOrmModule.forRoot({
    type: 'mysql',
    host: process.env.DATABASE_HOST,
    port: env.DATABASE_PORT,
    username: env.DATABASE_USERNAME,
    password: env.DATABASE_PASSWORD,
    database: env.DATABASE_NAME_TESTING,
    entities: [User, Blog],
    synchronize: true,
  }), AuthModule],
  controllers: [AppController],
  providers: [
    AppService,
    /*{
      provide: APP_GUARD,
      useClass: RolesGuard,
    },*/
    // Other providers and modules...
  ],
})
export class TestAppModule {}
