import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication, UnauthorizedException } from '@nestjs/common';
import * as request from 'supertest';
import { UserController } from '../src/modules/user/user.controller';
import { UserService } from '../src/modules/user/user.service';
import { User, UserRole } from '../src/modules/user/entities/user.entity';
import { UserRepository } from '../src/modules/user/user.repository';
import { UserRequestDto } from '../src/modules/user/dto/user-request.dto';
import { BlogRequestDto } from '../src/modules/blog/dto/blog-request.dto';
import { BlogService } from '../src/modules/blog/blog.service';
import { Blog } from '../src/modules/blog/entities/blog.entity';
import { TestAppModule } from './test.app.module';
import { BlogRepository } from '../src/modules/blog/blog.repository';
import { LogInDto } from '../src/modules/auth/dto/log-in.dto';
import { generateUsername } from 'unique-username-generator';
import { generate } from 'rxjs';

describe('E2E UserController', () => {
  let app: INestApplication;
  let moduleFixture: TestingModule;
  let targetMemberId;
  let targetAdminId;
  let memberBearerToken;
  let adminBearerToken;
  let targetBlogIdMember, targetBlogIdAdmin;
  let usernameMember, usernameAdmin;

  beforeAll(async () => {
    moduleFixture = await Test.createTestingModule({
      imports: [TestAppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    const blogRepository = moduleFixture.get<BlogRepository>(BlogRepository);
    blogRepository.clear();
    const userRepository = moduleFixture.get<UserRepository>(UserRepository);
    userRepository.clear();
  }, 100000);

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    
  });

  describe('POST /user', () => {
    it('should create a new user', async () => {
      usernameMember = generateUsername();
      usernameAdmin = generateUsername();
      const userRequestDtoMember: UserRequestDto = {
        username: usernameMember,
        password: 'testPassword',
        name: 'Test User',
        role: UserRole.Member,
      };

      const userRequestDtoAdmin: UserRequestDto = {
        username: usernameAdmin,
        password: 'testPassword2',
        name: 'Test User 2',
        role: UserRole.Admin,
      };

      const response = await request(app.getHttpServer()).post('/user').send(userRequestDtoMember).expect(HttpStatus.CREATED);
      const response2 = await request(app.getHttpServer()).post('/user').send(userRequestDtoAdmin).expect(HttpStatus.CREATED);
      targetMemberId = response.body.id;
      targetAdminId = response2.body.id;

      expect(response.body).toEqual({
        id: expect.any(Number),
        username: userRequestDtoMember.username,
        name: userRequestDtoMember.name,
        role: userRequestDtoMember.role,
        deletedAt: null
      });

      expect(response2.body).toEqual({
        id: expect.any(Number),
        username: userRequestDtoAdmin.username,
        name: userRequestDtoAdmin.name,
        role: userRequestDtoAdmin.role,
        deletedAt: null
      });
    });

    it('should throw BadRequestException for missing properties', async () => {
      await request(app.getHttpServer()).post('/user').send({}).expect(HttpStatus.BAD_REQUEST);
    });

    it('should throw BadRequestException for invalid username', async () => {
      const userRequestDto: UserRequestDto = {
        username: 'invalid username',
        password: 'testPassword',
        name: 'Test User',
        role: UserRole.Member,
      };

      await request(app.getHttpServer()).post('/user').send(userRequestDto).expect(HttpStatus.BAD_REQUEST);
    });
  });

  describe('LOGIN /auth/login', () => {
    it('should log in a user and return a token', async () => {
      const userRequestDtoMember: UserRequestDto = {
        username: usernameMember,
        password: 'testPassword',
        name: 'Test User',
        role: UserRole.Member,
      };
      const logInDtoMember: LogInDto = {
        username: userRequestDtoMember.username,
        password: userRequestDtoMember.password,
      };

      const response = await request(app.getHttpServer()).post('/auth/login').send(logInDtoMember).expect(HttpStatus.OK);

      memberBearerToken = response.body.access_token;
      
      expect(response.body).toHaveProperty('access_token');
      expect(response.body.access_token).toBeDefined();

      const userRequestDtoAdmin: UserRequestDto = {
        username: usernameAdmin,
        password: 'testPassword2',
        name: 'Test User 2',
        role: UserRole.Admin,
      };

      const logInDtoAdmin: LogInDto = {
        username: userRequestDtoAdmin.username,
        password: userRequestDtoAdmin.password,
      };

      const response2 = await request(app.getHttpServer()).post('/auth/login').send(logInDtoAdmin).expect(HttpStatus.OK);

      adminBearerToken = response2.body.access_token;
      
      expect(response2.body).toHaveProperty('access_token');
      expect(response2.body.access_token).toBeDefined();
    });

    it('should throw UnauthorizedException for invalid credentials', async () => {
      const logInDto: LogInDto = {
        username: usernameMember,
        password: 'wrongPassword',
      };

      await request(app.getHttpServer()).post('/auth/login').send(logInDto).expect(HttpStatus.UNAUTHORIZED);
    });

    it('should throw BadRequestException for missing properties', async () => {
      await request(app.getHttpServer()).post('/auth/login').send({}).expect(HttpStatus.BAD_REQUEST);
    });
  });

  describe('GET /user', () => {
    describe('Admin user', () => {
      it('should return all users for Admin role', async () => {
        const response = await request(app.getHttpServer())
          .get('/user')
          .set('Authorization', `Bearer ${adminBearerToken}`)
          .expect(200);
  
        expect(response.body).toHaveLength(2);
        response.body.forEach((user) => expect([UserRole.Admin, UserRole.Member]).toContain(user.role));
      });
    });
  
    describe('Non-Admin user', () => {
      it('should return users without Admin role for non-Admin user', async () => {
        const response = await request(app.getHttpServer())
          .get('/user')
          .set('Authorization', `Bearer ${memberBearerToken}`)
          .expect(200);
  
        expect(response.body).toHaveLength(1);
        response.body.forEach((user) => expect([UserRole.Member]).toContain(user.role));
      });
    });
  });

  describe('GET /user/:username', () => {
    it('should return a user by username', async () => {
      const userRequestDto: UserRequestDto = {
        username: usernameMember,
        password: 'testPassword',
        name: 'Test User',
        role: UserRole.Member,
      };

      const response = await request(app.getHttpServer()).get(`/user/${userRequestDto.username}`).expect(200);
      console.log(response.body);

      expect(response.body).toEqual({
        id: expect.any(Number),
        username: userRequestDto.username,
        name: userRequestDto.name,
        role: userRequestDto.role,
      });
    });

    it('should be 404 not found', async () => {
      const response = await request(app.getHttpServer()).get(`/user/userNameGagal`).expect(404);
    });
  });

  describe('GET /user/:id', () => {
    it('should return a specific user by ID', async () => {
      const userRequestDto: UserRequestDto = {
        username: usernameMember,
        password: 'testPassword',
        name: 'Test User',
        role: UserRole.Member,
      };
      const response2 = await request(app.getHttpServer()).get(`/user/${targetMemberId}`).expect(200);

      expect(response2.body).toEqual({
        id: targetMemberId,
        username: userRequestDto.username,
        name: userRequestDto.name,
        role: userRequestDto.role,
      });
    });

    it('should be 404 not found', async () => {
      const response = await request(app.getHttpServer()).get(`/user/3423423561`).expect(404);
    });
  });

  describe('PATCH /user/:id', () => {
    
    it('should allow an Admin user to update any user', async () => {

      const updateUserRequestDto = { name: 'Updated by Admin' };

      const response = await request(app.getHttpServer())
        .patch(`/user/${targetMemberId}`)
        .set('Authorization', `Bearer ${adminBearerToken}`)
        .send(updateUserRequestDto)
        .expect(HttpStatus.OK);
      const expectedDto = {
        username: usernameMember,
        id: targetMemberId,
        name: updateUserRequestDto.name,
        role: UserRole.Member,
      };
      expect(response.body).toEqual(expectedDto);
    });

    it('should allow a Member user to update their own user data', async () => {
      const updateUserRequestDto = { name: 'Updated by Admin 2' };

      const response = await request(app.getHttpServer())
        .patch(`/user/${targetMemberId}`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .send(updateUserRequestDto)
        .expect(HttpStatus.OK);
      const expectedDto = {
        username: usernameMember,
        id: targetMemberId,
        name: updateUserRequestDto.name,
        role: UserRole.Member,
      };
      expect(response.body).toEqual(expectedDto);
    });

    it('should not allow a Member user to update other users\' data', async () => {
      const updateUserRequestDto = { name: 'Updated by Admin 2' };

      const response = await request(app.getHttpServer())
        .patch(`/user/${targetAdminId}`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .send(updateUserRequestDto)
        .expect(HttpStatus.UNAUTHORIZED);
      expect(response.body.message).toEqual('You are not authorized to update this user.');
    });
  });  

  describe('POST /blog', () => {
    it('should create a new blog', async () => {
      const blogRequestDto: BlogRequestDto = {
        title: 'Test Title',
        content: 'Test Content',
      };

      const response = await request(app.getHttpServer())
        .post(`/blog`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .send(blogRequestDto)
        .expect(HttpStatus.CREATED);

      targetBlogIdMember = response.body.id;
  
      expect(response.body).toMatchObject({
        id: expect.any(Number),
        title: blogRequestDto.title,
        content: blogRequestDto.content,
        userId: targetMemberId,
      });

      const response2 = await request(app.getHttpServer())
        .post(`/blog`)
        .set('Authorization', `Bearer ${adminBearerToken}`)
        .send(blogRequestDto)
        .expect(HttpStatus.CREATED);

      targetBlogIdAdmin = response2.body.id;

      expect(response2.body).toMatchObject({
        id: expect.any(Number),
        title: blogRequestDto.title,
        content: blogRequestDto.content,
        userId: targetAdminId,
      });

    });
  });
  describe('GET /blog/:id', () => {
    it('should return a specific blog by ID', async () => {
      const blogRequestDto: BlogRequestDto = {
        title: 'Test Title',
        content: 'Test Content',
      };
      const response = await request(app.getHttpServer()).get(`/blog/${targetBlogIdMember}`).expect(HttpStatus.OK);
      expect(response.body).toMatchObject({
        id: targetBlogIdMember,
        title: blogRequestDto.title,
        content: blogRequestDto.content
      });
    });
  }); 

  describe('GET /blog', () => {
    it('should return all blogs', async () => {
      const blogRequestDto: BlogRequestDto = {
        title: 'Test Title',
        content: 'Test Content',
      };
      const response = await request(app.getHttpServer()).get('/blog').expect(HttpStatus.OK);
      expect(response.body).toHaveLength(2);
      expect(response.body[0]).toMatchObject({
        id: targetBlogIdMember,
        title: blogRequestDto.title,
        content: blogRequestDto.content,
      });
      expect(response.body[1]).toMatchObject({
        id: targetBlogIdAdmin,
        title: blogRequestDto.title,
        content: blogRequestDto.content,
      });
    });
  });

  describe('GET /blog/me', () => {
    it('should return all blogs', async () => {
      const blogRequestDto: BlogRequestDto = {
        title: 'Test Title',
        content: 'Test Content',
      };
      const response = await request(app.getHttpServer())
        .get(`/blog/me`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .expect(HttpStatus.OK)
      expect(response.body).toHaveLength(1);
      expect(response.body[0]).toMatchObject({
        id: targetBlogIdMember,
        title: blogRequestDto.title,
        content: blogRequestDto.content,
      });
    });
  });

  describe('PATCH /blog/:id', () => {
    it('should allow an Admin user to update any blog', async () => {
      const blogRequestDto: BlogRequestDto = {
        title: 'Test Upd Title',
        content: 'Test Upd Content',
      };
      const response = await request(app.getHttpServer())
        .patch(`/blog/${targetBlogIdMember}`)
        .set('Authorization', `Bearer ${adminBearerToken}`)
        .send(blogRequestDto)
        .expect(HttpStatus.OK);

      expect(response.body).toMatchObject({
        id: targetBlogIdMember, 
        userId: targetMemberId, 
        title: blogRequestDto.title, 
        content: blogRequestDto.content 
      });
    });

    it('should allow a Member user to update their own blog data', async () => {
      const blogRequestDto: BlogRequestDto = {
        title: 'Test Upd Title',
        content: 'Test Upd Content',
      };
      const response = await request(app.getHttpServer())
        .patch(`/blog/${targetBlogIdMember}`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .send(blogRequestDto)
        .expect(HttpStatus.OK);

      expect(response.body).toMatchObject({
        id: targetBlogIdMember, 
        userId: targetMemberId, 
        title: blogRequestDto.title, 
        content: blogRequestDto.content 
      });
    });

    it('should not allow a Member user to update other blogs\' data', async () => {
      const blogRequestDto: BlogRequestDto = {
        title: 'Test Upd Title',
        content: 'Test Upd Content',
      };
      const response = await request(app.getHttpServer())
        .patch(`/blog/${targetBlogIdAdmin}`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .send(blogRequestDto)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(response.body.message).toEqual("You don't have permission to update this blog");
    });
  });

  describe('DELETE /blog/:id', () => {
    it('should delete a blog for Admin role', async () => {

      const blogRequestDto: BlogRequestDto = {
        title: 'Test Title 2',
        content: 'Test Content 2',
      };

      const response2 = await request(app.getHttpServer())
        .post(`/blog`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .send(blogRequestDto)
        .expect(HttpStatus.CREATED);

      const response = await request(app.getHttpServer())
        .delete(`/blog/${response2.body.id}`)
        .set('Authorization', `Bearer ${adminBearerToken}`)
        .send()
        .expect(HttpStatus.OK);

      expect(response.text).toBe(`Deleted Blog with id ${response2.body.id}`);
    });

    it('should delete own blog for Member role', async () => {
      const response = await request(app.getHttpServer())
        .delete(`/blog/${targetBlogIdMember}`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .send()
        .expect(HttpStatus.OK);

      expect(response.text).toBe(`Deleted Blog with id ${targetBlogIdMember}`);
    });

    it('should not allow Member to delete another user\'s blog', async () => {

      const response = await request(app.getHttpServer())
        .delete(`/blog/${targetBlogIdAdmin}`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .expect(HttpStatus.UNAUTHORIZED);
    });
  });

  describe('DELETE /user/:id', () => {

    it('cant delete own user', async () => {
      const response = await request(app.getHttpServer())
        .delete(`/user/${targetMemberId}`)
        .set('Authorization', `Bearer ${memberBearerToken}`)
        .expect(HttpStatus.FORBIDDEN);
    });
    it('should delete a user by ID for Admin role', async () => {
      const response = await request(app.getHttpServer())
        .delete(`/user/${targetMemberId}`)
        .set('Authorization', `Bearer ${adminBearerToken}`)
        .expect(HttpStatus.OK);

      expect(response.text).toBe(`Deleted User with id ${targetMemberId}`);

    });
  });
});
